const path = require('path')

// Implement the Gatsby API 'onCreatePage'. This is
// called after every page is created.
exports.onCreatePage = async ({ page, actions }) => {
  const { createPage } = actions

  // Create client-only pages
  if (page.path.match(/^\/albums/)) {

    // page.matchPath is a special key that's used for matching pages
    // with corresponding routes only on the client.
    page.matchPath = "/albums/*"

    // Update the page.
    createPage(page)
  }

  if (page.path.match(/^\/volumes/)) {
    page.matchPath = "/volumes/*"
    createPage(page)
  }

  if (page.path.match(/^\/scan/)) {
    page.matchPath = "/scan/*"
    createPage(page)
  }
}

exports.onCreateWebpackConfig = ({ stage, actions }) => {
  actions.setWebpackConfig({
    resolve: {
      modules: [path.resolve(__dirname, 'src'), 'node_modules'],
    },
  })
}
