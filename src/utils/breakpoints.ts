import window from './window'
import _screens from '../../.screens.json'

interface Dictionary<T> {
  [Key: string]: T;
}

const screens: Dictionary<string> = _screens

export const getBreakpointValue = (key: string): number =>
  +screens[key].slice(0, screens[key].indexOf('px'))

export const currentBreakpoint = (): string => {
  const { innerWidth } = window
  const keys = Object.keys(screens)

  return (innerWidth >= getBreakpointValue(keys[keys.length - 1]))
    ? keys[keys.length - 1]
    : keys.reduce<string>(
      (acc, v, i) => {
        const val = getBreakpointValue(v)
        const valNext = getBreakpointValue(keys[i + 1] || '2xl')
        return  val < innerWidth  && innerWidth < valNext ? v : acc
      },
      'sm'
    )
}
