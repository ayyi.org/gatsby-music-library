/* eslint-disable no-empty */

function waitFor (condition, { timeout } = {}) {
  return new Promise(resolve => {
    let error

    let interval = setInterval(async () => {
      try {
        const result = await condition()
        if (result) {
          interval = clearInterval(interval)
          resolve()
        }
      } catch (e) { error = e }
    }, 200)

    timeout && setTimeout(() => {
      if (interval) interval = clearInterval(interval)
      if (error) console.log(error)
    }, timeout)
  })
}

export default waitFor
