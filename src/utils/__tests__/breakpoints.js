import { currentBreakpoint } from '../breakpoints'

describe('Breakpoints', () => {

  it('returns correct breakpoint', () => {
    global.innerWidth = 600
    expect(currentBreakpoint()).toEqual('sm')

    global.innerWidth = 800
    expect(currentBreakpoint()).toEqual('md')

    global.innerWidth = 1025
    expect(currentBreakpoint()).toEqual('lg')

    global.innerWidth = 1281
    expect(currentBreakpoint()).toEqual('xl')

    global.innerWidth = 4000
    expect(currentBreakpoint()).toEqual('2xl')
  })
})
