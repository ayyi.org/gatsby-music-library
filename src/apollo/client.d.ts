
import { ApolloClient, NormalizedCacheObject } from '@apollo/client'

export const client: ApolloClient<NormalizedCacheObject>
