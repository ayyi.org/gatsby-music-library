/* istanbul ignore file */
import fetch from 'cross-fetch'
import { ApolloClient, InMemoryCache, defaultDataIdFromObject, HttpLink, split } from '@apollo/client'
import { WebSocketLink } from '@apollo/client/link/ws'
import { getMainDefinition } from '@apollo/client/utilities'
import config from '../../.config.json'

const is_ssr = typeof window === 'undefined'

const httpLink = new HttpLink({
  uri: config.backend,
  fetch,
})
const host = config.backend.replace('http://', '')

/*
 *  connect to Mp3db apollo server
 */
export const client = new ApolloClient({
  link: is_ssr
    ? httpLink
    : split(
      ({ query }) => {
        const definition = getMainDefinition(query)
        return (
          definition.kind === 'OperationDefinition' && definition.operation === 'subscription'
        )
      },
      new WebSocketLink({
        uri: `ws://${host}/subscriptions`,
        options: {
          reconnect: true
        }
      }),
      httpLink
    ),
  cache: new InMemoryCache({
    typePolicies: {
      updateTrackArtist_Volume: { keyFields: ['name']},
      updateTrackArtist_Track: { keyFields: ['id']},
      updateTrackArtist_Album: { keyFields: ['album_id']}
    },
    dataIdFromObject: object => {
      switch (object.__typename) {
        case 'Album':
          // Album items in the cache will be keyed with Album1, Album2, etc
          return 'Album' + object.album_id
        default:
          return defaultDataIdFromObject(object)
      }
    }
  })
})

export default client
