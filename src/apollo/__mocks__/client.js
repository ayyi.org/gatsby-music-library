const mock = require('../../../__mocks__/graphql-server')

const client = jest.createMockFromModule('../client')

client.default.mutate = function (q, v) {
  return mock.client.mutate(q, v)
}

module.exports = client
