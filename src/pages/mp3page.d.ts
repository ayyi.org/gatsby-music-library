import { PageProps } from 'gatsby'

export interface Mp3PageProps extends PageProps {
  data: {
    site: {
      siteMetadata: {
        darkMode: boolean
      }
    }
  }
}
