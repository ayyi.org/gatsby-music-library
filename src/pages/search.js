import React from 'react'
import PropTypes from 'prop-types'
import { useLazyQuery } from '@apollo/client'
import windowGlobal from 'utils/window'
import { SEARCH } from 'queries'
import Layout from 'components/layout'
import Album from 'components/album'

const results = data => {
  if (!data.tracks?.length) return <p>Empty result set</p>

  const albums = data.tracks.reduce(
    (acc, v) => {
      acc[v.album] ||= []
      acc[v.album].push(v)
      return acc
    },
    {}
  )

  const artist = tracks =>
    tracks.reduce(
      (acc, v) => (v.artist !== acc || v.artist.toLowerCase() === 'various') ? 'va' : acc,
      tracks[0].artist
    )

  const items = () => Object.keys(albums)
    .sort((a, b) => {
      // sort by artist, albumdate, volume

      const track_a = albums[a][0]
      const track_b = albums[b][0]

      const _artist_a = (track_a.albumObj?.artist || artist(albums[a])).toLowerCase()
      const _artist_b = (track_b.albumObj?.artist || artist(albums[b])).toLowerCase()

      if (_artist_a === _artist_b) {

        if (track_a.album === track_b.album) {
          return track_a.volume > track_b.volume ? 1 : -1
        }

        if (_artist_a === 'va' && _artist_b === 'va') {
          // va albums are not sorted by date
          return track_a.album.toLowerCase() > track_b.album.toLowerCase() ? 1 : -1
        }

        const date_a = track_a.year
        const date_b = track_b.year
        if (date_b === date_a) {
          return track_a.album.toLowerCase() > track_b.album.toLowerCase() ? 1 : -1
        }
        return date_a > date_b ? 1 : -1
      }
      return _artist_a > _artist_b.toLowerCase() ? 1 : -1
    })
    .sort((a, b) =>
      // put whole albums before singles
      // (this is intended to work when searching by artist)

      (albums[b].length > 5 && albums[a].length <= 5)
        ? 1
        : (albums[a].length > 5 && albums[b].length <= 5)
          ? -1
          : 0
    )
    .map(album =>
      <li key={album}>
        <Album name={album} tracks={albums[album]}/>
      </li>
    )

  return <>
    <p>Showing {data.tracks?.length} Items</p>
    <ul style={{display: 'flex', flexWrap: 'wrap'}}>{items()}</ul>
  </>
}

const Search = ({uri}) => {
  const [getData, { loading, error, data }] = useLazyQuery(SEARCH)
  const [search, setSearch] = React.useState('')

  React.useEffect(() => {
    setSearch(windowGlobal.localStorage.getItem('search') || '')
  }, [])

  React.useEffect(() => {
    windowGlobal.localStorage.setItem('search', search)
  }, [search])

  return <Layout uri={uri} >
    <h1 style={{marginTop: 5}}>Search</h1>
    <form
      onSubmit={
        e => {
          getData({variables: {search: search}})
          e.preventDefault()
        }
      }
    >
      <input type="text" placeholder="Search" value={search} onChange={e => setSearch(e.target.value)}/>
    </form>

    <h2 className="screen-hidden">Results</h2>
    {loading && <p>Loading ...</p>}
    {error && <p>Error: {error.message}</p>}
    {data && results(data)}
  </Layout>
}

Search.propTypes = {
  uri: PropTypes.string
}

export default Search
