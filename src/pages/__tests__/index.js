import React from 'react'
import renderer from 'react-test-renderer'
import { StaticQuery } from 'gatsby'
import Helmet from 'react-helmet'
import loadable from '@loadable/component'
import { ApolloProvider } from '@apollo/client'
import waitFor from 'utils/waitfor'
import { client } from '../../../__mocks__/graphql-server'
import Index from '../index'
import Layout from 'components/layout'
import VolumeThumbnail from 'components/volume-thumbnail'

const Charts = loadable(() => Promise.resolve().then(() => __importStar(require('../../components/charts' /* webpackChunkName: "D3" */))))

jest.mock('../../components/timer')
jest.mock('../../components/charts')
jest.mock('react-transition-group')

beforeEach(() => {
  StaticQuery.mockImplementationOnce(({ render }) =>
    render({
      site: {
        siteMetadata: {
          title: 'Title',
        }
      }
    })
  )
})

describe('Index', () => {
  let instance
  let tree

  beforeAll(async () => {
    await Charts.load()

    tree = renderer.create(<ApolloProvider client={client}><Index /></ApolloProvider>)
    instance = tree.root
  })

  it('renders correctly', async () => {
    const spy = jest.spyOn(console, 'error')
    spy.mockImplementation(() => {})

    await waitFor(() =>
      {
        const volumes = instance.find(el => el.props.id === 'latest-volumes')
        const volume_items = volumes.findAll(el => el.type === 'li')
        expect(volume_items).toHaveLength(5)
        expect(instance.find(el => el.props.id === 'latest-albums').findAll(el => el.type === 'li')).toHaveLength(2)

        return instance.find(el => el.props.id === 'chart-1')
      },
      {timeout: 5_000}
    )

    spy.mockRestore()

    expect(tree.toJSON()).toMatchSnapshot()

    const layout = instance.findByType(Layout).props
    const h1 = layout.children[0]
    expect(h1.type).toEqual('h1')

  }, 7_000)

  it('has correct metatags', () => {
    const helmet = Helmet.peek()

    expect(helmet.bodyAttributes.toComponent().className).toContain('home-page')
    expect(helmet.title.toComponent()[0].key).toBe('Mp3db')
  })

  it('shows latest volumes', () => {
    const volumes = instance.find(el => el.props.id === 'latest-volumes')

    const h2 = volumes.find(el => el.type === 'h2')
    expect(h2.children).toEqual(['Latest volumes'])

    const volume_items = volumes.findAll(el => el.type === 'li')
    expect(volume_items).toHaveLength(5)

    const thumbnails = volumes.findAllByType(VolumeThumbnail)
    expect(thumbnails).toHaveLength(5)
    thumbnails.forEach(thumbnail => {
      const images = thumbnail.findAllByType('img')
      expect(images).toHaveLength(1)
      const links = thumbnail.findAllByType('a')
      expect(links).toHaveLength(1)
    })
  })
})
