import React from 'react'
import renderer from 'react-test-renderer'
import Helmet from 'react-helmet'
import { ApolloProvider } from '@apollo/client'
import waitFor from 'utils/waitfor'
import { client } from '../../../__mocks__/graphql-server'
import Search from '../search'
import Layout from 'components/layout'
import Album from 'components/album'

const { act } = renderer

global.localStorage = {
  getItem: () => {},
  setItem: () => {}
}

describe('Search page', () => {

  const tree = renderer.create(<ApolloProvider client={client}><Search uri='/search'/></ApolloProvider>)
  const instance = tree.root

  it('renders correctly', async () => {
    expect(tree.toJSON()).toMatchSnapshot()

    const layout = instance.findByType(Layout).props
    const h1 = layout.children[0]
    expect(h1.type).toEqual('h1')
  })

  it('has metatags', () => {
    const helmet = Helmet.peek()

    expect(helmet.bodyAttributes.toComponent().className).toContain('search-page')
    expect(helmet.title.toComponent()[0].key).toBe('Mp3db')
  })

  it('performs search', async () => {
    const input = instance.findByProps({placeholder: 'Search'})

    await act(async () => {}) // wait for effects to run

    await act(async () => {
      input.props.onChange({target: {value: 'Hello'}})
    })

    expect(input.props.value).toEqual('Hello')

    const form = instance.findByType('form')
    await act(async () => {
      form.props.onSubmit({preventDefault: () => {}})
    })

    await waitFor(() => {
      const albums = instance.findAll(el => el.type === Album)
      return albums.length > 0
    })
  })

  it('has expand button', async () => {
    const expand = instance.find(v => v.props.children === '⌄')

    const n_tracks = instance.findAllByType('li').length
    expect(n_tracks).toBeGreaterThan(5)

    await act(async () => {
      expand.props.onClick({preventDefault: () => {}})
    })

    const n_tracks2 = instance.findAllByType('li').length
    expect(n_tracks2).toBeGreaterThan(n_tracks)
  })
})
