import React from 'react'
import renderer from 'react-test-renderer'
import Helmet from 'react-helmet'
import waitFor from 'utils/waitfor'
import { ServerLocation } from '@reach/router'
import { ApolloProvider } from '@apollo/client'
import CTE from 'react-click-to-edit'
import { ALBUM } from 'queries'
import { client } from '../../../__mocks__/graphql-server'
import Albums from '../albums'
import { store } from '../../../__mocks__/graphql-server'

jest.mock('../../apollo/client')
jest.mock('../../hooks/useTimedQuery')

describe('Albums page', () => {
  let instance

  it('renders correctly', async () => {
    const tree = renderer.create(
      <ServerLocation url={'/albums/'}>
        <ApolloProvider client={client}>
          <Albums uri='/albums/'/>
        </ApolloProvider>
      </ServerLocation>
    )
    instance = tree.root

    await renderer.act(async () => {})

    await waitFor(() => {
      const list = instance.find(v => v.props.id === 'albums')
      return list.props.children.length > 1
    })

    const h1 = instance.findByType('h1')
    expect(h1.children[0]).toEqual('Albums')

    expect(tree.toJSON()).toMatchSnapshot()
  })

  it('has metatags', () => {
    const helmet = Helmet.peek()

    expect(helmet.bodyAttributes.toComponent().className).toContain('albums-page')
    expect(helmet.title.toComponent()[0].key).toBe('Mp3db')
  })

  it('shows info', async () => {
    const button = instance.findAllByType('button')

    await renderer.act(async () => {
      button[0].props.onClick()
    })

    await renderer.act(async () => {})

    await waitFor(() => instance.find(v => v.props.id === 'info'))
  })
})

describe('Album detail page', () => {
  let instance

  it('renders', async () => {
    const tree = renderer.create(<ServerLocation url={'/albums/1'}><ApolloProvider client={client}><Albums uri='/albums/1'/></ApolloProvider></ServerLocation>)
    instance = tree.root

    await renderer.act(async () => {})

    expect(tree.toJSON()).toMatchSnapshot()
  })

  it('mutates album artist', async () => {
    const id = 1

    let edit
    await waitFor(() => {
      edit = instance.find(v => v.props.id === 'artist-edit')
      return edit.props.children.type === CTE
    })

    const cte = edit.props.children//await get_cte()

    await renderer.act(async () => {
      cte.props.endEditing('New Artist')
    })

    await waitFor(() => {
      return store.store.Album[id].artist === 'New Artist'
    })

    // check cache has been updated
    const { data: { albums }, errors } = await client.query({query: ALBUM, variables: { id }})
    expect(errors).toEqual(undefined)
    expect(albums.albums[0].artist).toEqual('New Artist')
  })

  it('mutates album year', async () => {
    const id = 1

    let edit
    await waitFor(() => {
      edit = instance.find(v => v.props.id === 'date-edit')
      return edit.props.children.type === CTE
    })

    const cte = edit.props.children

    await renderer.act(async () => {
      cte.props.endEditing('2022')
    })

    await waitFor(() => {
      const track_ref = store.store.Album[id].tracks[0]
      const track = store.store[track_ref.$ref.typeName][track_ref.$ref.key]
      return track.year === '2022-01-01'
    })

    // check cache has been updated
    const { data: { albums }, errors } = await client.query({query: ALBUM, variables: { id }})
    expect(errors).toEqual(undefined)
    expect(albums.albums[0].tracks[0].year).toEqual('2022')
  })
})
