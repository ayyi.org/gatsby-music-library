import React from 'react'
import renderer from 'react-test-renderer'
import { ApolloProvider } from '@apollo/client'
import { ServerLocation } from '@reach/router'
import waitFor from 'utils/waitfor'
import { client } from '../../../__mocks__/graphql-server'
import Volumes from '../volumes'
import { useQuery } from '@apollo/client'

/*
 *  Mock useTimedQuery such that it always returns a time of zero
 */
jest.mock('../../hooks/useTimedQuery')
import useTimedQuery from '../../hooks/useTimedQuery'
useTimedQuery.mockImplementation((q, v) => {
  return {...useQuery(q, v), time: 0}
})

describe('Volumes page', () => {
  let instance

  it('renders', async () => {
    let tree = renderer.create(
      <ServerLocation url={'/volumes/'}>
        <ApolloProvider client={client}>
          <Volumes uri='/volumes/'/>
        </ApolloProvider>
      </ServerLocation>
    )
    instance = tree.root

    await renderer.act(async () => {})

    await waitFor(() =>
      {
        const list = instance.findAllByType('li')
        return list.length > 1
      }
    )

    const h1 = instance.findByType('h1')
    expect(h1.children[0]).toEqual('Volumes')

    expect(tree.toJSON()).toMatchSnapshot()
  })

  it('previous page', async () => {
    const prev = instance.find(v => ~v.props.className?.indexOf('btn') && v.props.children === '<')
    prev.props.onClick()
  })

  it('next page', async () => {
    const next = instance.find(v => ~v.props.className?.indexOf('btn') && v.props.children === '>')
    next.props.onClick()
  })
})

describe('Volume detail page', () => {
  let instance

  it('renders', async () => {
    let tree = renderer.create(
      <ServerLocation url={'/volumes/1'}>
        <ApolloProvider client={client}>
          <Volumes uri='/volumes/1'/>
        </ApolloProvider>
      </ServerLocation>
    )
    instance = tree.root

    await renderer.act(async () => {})

    await waitFor(() =>
      {
        const list = instance.findAllByType('li')
        return list.length > 0
      }
    )

    const h1 = instance.findByType('h1')
    expect(h1.children[0]).toContain('Volume')

    expect(tree.toJSON()).toMatchSnapshot()
  })
})
