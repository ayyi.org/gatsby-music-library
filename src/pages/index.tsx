import React from 'react'
import Layout from '../components/layout'
import Panels from '../components/panels/panels'
import { Mp3PageProps } from './mp3page'
import loadable from '@loadable/component'

const Charts = loadable(() => import('../components/charts'/* webpackChunkName: "D3" */))

export default function Home({ uri }: Mp3PageProps) {

  return <Layout uri={uri}>
    <h1>Home</h1>
    <Panels types={['Latest volumes', 'Latest albums']}/>
    <section id="stats">
      <h2>Stats</h2>
      <Charts/>
    </section>
  </Layout>
}

