import React, { useEffect } from 'react'
import { navigate } from 'gatsby'
import { Mp3PageProps } from './mp3page'
import Layout from '../components/layout'
import { useQuery, useLazyQuery, useSubscription, ApolloCache } from '@apollo/client'
import { client } from '../apollo/client'
import { VOLUME, SCAN, FILE_FEED, FILE_REMOVED, ALBUM_UPDATED, ALBUM_REMOVED, WRITE } from '../queries'
import Volume from '../components/volume-compact'
import tw, { styled } from 'twin.macro'

const Grid = styled.div`
  display: grid;
  grid-template-columns: 2fr 1fr
`

const Left = styled.section`
  display: grid-item;
`

const TrackList = styled.ul`
  clear: both;
  padding-right: 10px;
  .in-library {
    &:after {
      content: '✅';
    }
  }
`

const Filename = styled.span`
  ${tw`inline-block text-gray-400 truncate mr-4`}
  width: 300px;
  max-width: 300px;
`

const Artist = styled.span`
`
const ArtistTitle = styled.span`
  ${tw`inline-block`}
  min-width: 400px;
`
const Songnb = styled.span`
  display: inline-block;
  min-width: 2em;
`
const Time = styled.span`
  min-width: 3em;
  ${tw`inline-block text-right mr-2`}
`
const Thumbnail = styled.div`
  ${tw`float-left mr-2 mb-2`}
  transition: height 0.5s;
`

interface Scan {
  albums: AlbumIn[]
}

interface Track {
  id: number
  filename: string
  songnb: number
  name: string
  artist: string
  time: string
}

interface AlbumIn {
  album_id: number
  name: string
  tracks: Track[]
}

interface Album {
  artist?: string
  name?: string
  year?: string
  tracks: Track[]
  thumbnail?: string
}

interface Albums {
   [key: string]: Album
}

export default function ScanPage({ uri, location }: Mp3PageProps) {
  const [volume, setVolume] = React.useState<string>('033')

  const vol = (() => {
    const path = location?.pathname || ''
    const split = path.split('/')
    if (split.length === 3) {
	  return split[2]
    }
    return volume
  })()

  const [tracks, setTracks] = React.useState<Albums>({})

  const { data: volumeData } = useQuery(VOLUME, {variables: {id: volume}})

  const [getData, { loading, error }] = useLazyQuery(
    SCAN, {
      variables: {id: vol},
      onCompleted: ({ scan }: { scan: Scan }) => {
        scan.albums.forEach(album => {
          tracks[album.album_id] ||= {name: album.name, tracks: []}
          const _tracks = tracks[album.album_id].tracks || []
          tracks[album.album_id] = Object.assign(tracks[album.album_id] || {}, album)
          tracks[album.album_id].tracks = _tracks.concat(album.tracks || [])
        })
        setTracks(tracks)
      }
    }
  )

  useEffect(() => {
    getData()
  }, [vol])

  useSubscription(
    FILE_FEED,
    {onSubscriptionData: ({ subscriptionData: { data: { fileAdded } }}) => {
      const { album_id } = fileAdded

      tracks[album_id] ||= {tracks: []}
      tracks[album_id].tracks.push(fileAdded)

      setTracks(tracks)
    }}
  )

  useSubscription(
    FILE_REMOVED,
    {onSubscriptionData: ({ subscriptionData: { data: { fileRemoved } }}) => {
      const { id, album_id } = fileRemoved

      console.log('FILE_REMOVED id=', id, album_id)
      if (tracks[album_id]) {
        tracks[album_id].tracks = [] // TODO only remove one track

        setTracks(tracks)
      }
    }}
  )

  useSubscription(
    ALBUM_UPDATED,
    {onSubscriptionData: ({ subscriptionData: { data: { albumUpdated: album } }}) => {
      const { album_id } = album

      tracks[album_id] ||= {tracks: []}
      tracks[album_id] = Object.assign(tracks[album_id] || {tracks: []}, album)
      setTracks(tracks)
    }}
  )

  useSubscription(
    ALBUM_REMOVED,
    {onSubscriptionData: ({ subscriptionData: { data: { albumRemoved: album } }}) => {
      const { album_id } = album

      if (tracks[album_id]) {
        delete tracks[album_id]
        setTracks(tracks)
      }
    }}
  )

  const write = () => {
    client.mutate({
      mutation: WRITE,
      variables: {volume: vol},
      update (cache: ApolloCache<any>) {
        const id = cache.identify({ id: vol, __typename: 'Volume' })
        cache.evict({ id })
        cache.gc()
      },
      refetchQueries: [{
        query: VOLUME,
        variables: {id: vol}
      }]
    })
  }


  const track = (t: Track, inLibrary: boolean) =>
    <li key={t.id} className={inLibrary ? 'in-library' : ''}>
      <Filename>{t.filename}</Filename>
      <ArtistTitle><Songnb>{t.songnb}</Songnb><Artist className="text-yellow-200 pr-2">{t.artist}</Artist>{t.name}</ArtistTitle><Time>{t.time}</Time>
    </li>

  return <Layout uri={uri}>
    <h1>Scan</h1>
    <Grid>
      <Left>
        <form
          onSubmit={
            e => {
              navigate(`/scan/${vol}`)
              e.preventDefault()
            }
          }
        >
          <input type="text" placeholder="Volume" value={vol} onChange={e => setVolume(e.target.value)}/>
          <button>Scan</button>
        </form>

        {error && <div>{error.message}</div>}
        {loading && <div>Loading...</div>}

        {tracks && Object.keys(tracks).map(album_id => {
          const album = tracks[album_id]
          const libAlbum = volumeData?.volumes.volumes[0].albums?.find((v: Album) => v.name === album.name)
          return <div key={album_id}>
            {album.thumbnail && <Thumbnail><img src={album.thumbnail} alt="album thumbnail"/></Thumbnail>}
            <h2><span className="text-yellow-200">{album.artist}</span> {album.name}</h2>
            {album.year && <div>{album.year}</div>}
            {
              <TrackList>
                {album.tracks.map(t => {
                  const inLibrary = libAlbum?.tracks.find((l: Track) => l.songnb === t.songnb)
                  return track(t, inLibrary)
                })
                }
              </TrackList>
            }
          </div>
          }
        )}
        {Object.keys(tracks || {}).length > 0 && <button onClick={write}>Write</button>}
      </Left>
      <Volume id={vol}/>
    </Grid>
  </Layout>
}
