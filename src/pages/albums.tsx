import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'
import { Router } from '@reach/router'
import { RouteComponentProps } from '@reach/router'
import { useQuery, useLazyQuery } from '@apollo/client'
import Layout from '../components/layout'
import styled from 'styled-components'
import { Album as AlbumT } from '../graphql-types'
import { ALBUMS, ALBUM } from '../queries'
import Album from '../components/dbalbum'

const List = styled.ul`
  display: flex;
  flex-flow: column nowrap;
`

const Item = styled.li`
  display: flex;
  flex-flow: row nowrap;
`

const Name = styled.span`
  display: flex;
  flex-flow: row nowrap;
  min-width: 300px;
  padding: 0 4px;
`

const Albums = ({uri}: RouteComponentProps) => {
  const LandingPage = (p: RouteComponentProps) => {
    const { data: albums_data } = useQuery(ALBUMS)

    const albums: AlbumT[] = albums_data?.albums.albums || []

    const [getData, { loading, error, data }] = useLazyQuery(ALBUM)

    const rows = albums.map(node =>
      <Item key={node.album_id}>
        <Link to={`/albums/${node.album_id}`} className='album'>{node.album_id}</Link>
        <Name>{node.name}</Name>
        <button
          onClick={
            () => getData({variables: {id: node.album_id}})
          }
        >
        Show
        </button>
      </Item>
    )

    const results = (album: AlbumT) => {
      return <div id="info" key={album.album_id}>{album.artist || 'NO ARTIST'} - {album.name} ({album.album_id}) {album.tracks.length} tracks</div>
    }

    return <>
      <h1>Albums</h1>
      {loading && <p>Loading ...</p>}
      {error && <p>Error: ${error.message}</p>}
      {data && results(data.albums.albums[0])}
      <List id='albums'>
        {rows}
      </List>
    </>
  }

  return <Layout uri={uri as string}>
    <Router>
      <Album path="/albums/:id" />
      <LandingPage path="/albums/" />
    </Router>
  </Layout>
}

Albums.propTypes = {
  data: PropTypes.shape({
    allMysqlAlbum: PropTypes.shape({
      edges: PropTypes.array
    })
  }),
  uri: PropTypes.string
}

export default Albums
