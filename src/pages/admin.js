import React from 'react'
import { Link } from 'gatsby'
import PropTypes from 'prop-types'
import Layout from '../components/layout'
import { useLazyQuery, useMutation } from '@apollo/client'
import { GET, DATA, ALBUM_ORPHANS, DELETE_ALBUM_ALBUM, NO_ALBUM, FIX_ALBUM_ALBUM } from '../queries'
import styled from 'styled-components'

const AdminSection = styled.section`
  margin: 15px 0;
`

const Results = styled.div`
  display: grid;
  grid-template-columns: 50px 280px 100px;
`

const Admin = ({uri}) => {
  const [getData, { loading, error, data, refetch }] = useLazyQuery(ALBUM_ORPHANS)
  const [deleteAlbum, del] = useMutation(DELETE_ALBUM_ALBUM)

  const orphans = ({albumOrphans}) => {
    return <AdminSection>
      <h3 style={{margin: '0 0 10px'}}>Album table orphans</h3>
      {albumOrphans.length
        ? <Results>{ albumOrphans.map((v, i) =>
          <React.Fragment key={i}>
            <div>{v.album_id}</div>
            <div>{v.name}</div>
            <button
              onClick={() =>
                deleteAlbum({variables: {id: v.album_id}})
                  .then(() => refetch ? refetch() : getData())
              }
            >
            Delete
            </button>
          </React.Fragment>
        )}
        </Results>
      : <p>No orphans found</p>}
    </AdminSection>
  }

  const noAlbumQuery = useLazyQuery(NO_ALBUM)
  const [fixAlbum] = useMutation(FIX_ALBUM_ALBUM)

  const no_album = ({ data, error, refetch: refetch2 }) => {
    if (!data) return
    const { tracksWithoutAlbum } = data;
    return <AdminSection>
      <h3 style={{margin: '0 0 10px'}}>Tracks with missing album</h3>
      {error
        ? <p>Data error: {error.toString()}</p>
        : tracksWithoutAlbum?.length
        ? <Results>
            {tracksWithoutAlbum.map(v =>
              <React.Fragment key={v.id}>
                <div>{v.id}</div>
                <div><Link to={`/volumes/${v.volume}`}>{v.volume}</Link> {v.name} ({v.album})</div>
                <button
                  onClick={() =>
                    fixAlbum({variables: {id: v.id}})
                      .then(() => refetch2 ? refetch2() : noAlbumQuery[GET]())
                  }
                >
                Fix
                </button>
              </React.Fragment>
            )}
          </Results>
        : (<>
          <p>No tracks with missing album found</p>
        </>)
      }
    </AdminSection>
  }

  return <Layout uri={uri}>
    <h1>Admin</h1>
    <button
      className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded border-0"
      onClick={() => {getData(); noAlbumQuery[GET]()}}>
      Check database
    </button>
    {loading && <p>Loading...</p>}
    {error && <p>Failed to fetch data</p>}
    {data && orphans(data)}
    {del?.data?.deleteAlbumAlbum === false && <p>Delete Error</p>}
    {del?.data?.deleteAlbumAlbum === true && <p>Delete Ok</p>}
    {no_album(noAlbumQuery[DATA])}
  </Layout>
}

Admin.propTypes = {
  uri: PropTypes.string
}

export default Admin
