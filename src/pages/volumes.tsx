import React from 'react'
import { Router, RouteComponentProps } from '@reach/router'
import Layout from 'components/layout'
import { VOLUMES } from 'queries'
import useTimedQuery from 'hooks/useTimedQuery'
import { Volume } from '../graphql-types'
import VolumeThumbnail from 'components/volume-thumbnail'
import { VolumeThumbnailType } from 'components/volume-thumbnail'
import VolumePage from 'components/volume'
import { Mp3PageProps } from './mp3page'

const LandingPage: React.FC<RouteComponentProps> = () => {
  const { data, variables, refetch, time } = useTimedQuery(VOLUMES, {first: '590'} as any)

  const volumes: Volume[] = data?.volumes.volumes

  const change_page = (diff: number) =>
    refetch({first: '' + (+(variables as any).first + diff)})

  const show = () => <>
    <p>Total {data.volumes.pager.count} volumes</p>
    <ul className='flow-root'>{volumes.map((v: VolumeThumbnailType) => <VolumeThumbnail key={v.id} props={v}/>)}</ul>
    <p>Query took {((time as number)/1000).toFixed(2)}s</p>
    <span className="btn btn-blue" onClick={() => change_page(-20)}>&lt;</span>
    <span className="btn btn-blue" onClick={() => change_page(+20)}>&gt;</span>
  </>

  return <>
    <h1>Volumes</h1>
    {volumes && show()}
  </>
}

const Volumes = ({ uri }: Mp3PageProps) => {
  return <Layout uri={uri}>
    <Router>
      <VolumePage path='/volumes/:id' />
      <LandingPage path='/volumes/' />
    </Router>
  </Layout>
}

export default Volumes
