import React from 'react'
import { DocumentNode } from 'graphql'
import { useQuery, QueryResult, OperationVariables } from '@apollo/client'

type Variables = Record<"id", string>

interface useTimedQueryOut extends QueryResult {
  time: number
}

function useTimedQuery (gql_query: DocumentNode, variables: Variables) : useTimedQueryOut {
  const query = useQuery<any, OperationVariables>(gql_query, { variables })

  return {
    ...query,
    time: 0
  }
}

export default jest.fn(useTimedQuery)

