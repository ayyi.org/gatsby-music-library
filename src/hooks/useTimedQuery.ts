import React from 'react'
import { DocumentNode } from 'graphql'
import { useQuery, QueryResult, OperationVariables } from '@apollo/client'

type Variables = {
  id: number | string
}

interface useTimedQueryOut extends QueryResult {
  time: number | null | undefined
}

let first_run

function useTimedQuery (gql_query: DocumentNode, variables: Variables) : useTimedQueryOut {
  const query = useQuery<any, OperationVariables>(gql_query, {
    variables,
    onCompleted: () => {
      time || setTime(+new Date() - +start_time.current.time)
    }
  })

  const start_time = React.useRef({id: variables.id, time: new Date()})
  const [time, setTime] = React.useState<number | null>()

  if(start_time.current.id !== variables.id){
    if(time){
      start_time.current = {id: variables.id, time: new Date()}
      setTime(null)
    }
  }

  return {
    ...query,
    time
  }
}

export default useTimedQuery
