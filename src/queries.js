import gql from 'graphql-tag'

export const GET = 0;
export const DATA = 1;

export const ALBUMS = gql`
  query ($limit: Int) {
    albums(limit: $limit) {
      albums {
        album_id
        name
        thumbnail
      }
    }
  }
`

export const ALBUM = gql`
  query ($id: Int) {
    albums(id: $id) {
      albums {
        artist
        name
        album_id
        format
        tracks {
          id
          songnb
          name
          artist
          volume
          year
          min
          sec
        }
      }
    }
  }
`

export const SEARCH = gql`
  query searchQuery($search: String) {
    tracks(search: $search) {
      id
      songnb
      album
      artist
      name
      volume
      year
      min
      sec
      albumObj {
        album_id
        artist
      }
    }
  }
`

export const VOLUMES = gql`
    query volumes($first: String) {
      volumes(first: $first) {
        volumes {
          id
          thumbnail
        }
        pager {
          count
        }
      }
    }
  `

export const VOLUME = gql`
  query volume($id: ID) {
    volumes(id: $id) {
      volumes {
        albums {
          album_id
          name
          artist
          tracks {
            id
            songnb
            name
            artist
            year
            min
            sec
            albumObj {
              album_id
              artist
            }
          }
        }
      }
    }
  }
`

export const LATEST_VOLUMES = gql`
  query latestVolumes {
    latestVolumes {
      id
      thumbnail
    }
  }
`

export const SCAN = gql`
  query scan($id: String) {
    scan(id: $id) {
      albums {
        album_id
        name
        artist
        thumbnail
        tracks {
          id
          filename
          songnb
          name
          artist
          time
        }
      }
    }
  }
`

export const FILE_FEED = gql`
  subscription FileFeed {
    fileAdded {
      id
      songnb
      name
      artist
      time
      filename
      album_id
    }
  }
`

export const ALBUM_UPDATED = gql`
  subscription albumFeed {
    albumUpdated {
      album_id
      name
      artist
      year
      thumbnail
    }
  }
`

export const FILE_REMOVED = gql`
  subscription FileRemoved {
    fileRemoved {
      id
      album_id
    }
  }
`

export const ALBUM_REMOVED = gql`
  subscription AlbumRemoved {
    albumRemoved {
      album_id
    }
  }
`

export const WRITE = gql`
  mutation write($volume: String) {
    import(id: $volume) {
      id
    }
  }
`

export const STATS = gql`
  query stats {
    stats {
      genres {
        name
        count
      }
    }
  }
`

export const SET_ALBUM_NAME = gql`
  mutation updateAlbumName($id: Int, $name: String) {
    updateAlbumName(album_id: $id, name: $name) {
      name
    }
  }
`

export const SET_ALBUM_ARTIST = gql`
  mutation updateAlbumArtist($id: Int, $artist: String) {
    updateAlbumArtist(album_id: $id, artist: $artist) {
      name
    }
  }
`

export const SET_ALBUM_YEAR = gql`
  mutation updateAlbumYear($id: Int, $year: String) {
    updateAlbumYear(album_id: $id, year: $year) {
      name
    }
  }
`

export const SET_ALBUM_VOLUME = gql`
  mutation updateAlbumVolume($id: Int, $volume: String) {
    updateAlbumVolume(album_id: $id, volume: $volume) {
      name
    }
  }
`

export const SET_TRACK_ARTIST = gql`
  mutation updateTrackArtist($id: Int, $artist: String) {
    updateTrackArtist(id: $id, artist: $artist) {
      name
    }
  }
`

export const SET_TRACK_NAME = gql`
  mutation updateTrackName($id: Int, $name: String) {
    updateTrackName(id: $id, name: $name) {
      name
    }
  }
`

export const ALBUM_ORPHANS = gql`
  query {
    albumOrphans {
      album_id
      name
    }
  }
`

export const NO_ALBUM = gql`
  query tracksWithoutAlbum($volume: String) {
    tracksWithoutAlbum(volume: $volume) {
      id
      volume
      name
      album
    }
  }
`

export const ADD_ALBUM = gql`
  mutation addAlbum($name: String, $artist: String, $volume: String) {
    addAlbum(album: {name: $name, artist: $artist, volume: $volume})
  }
`

export const DELETE_ALBUM_ALBUM = gql`
  mutation deleteAlbumAlbum($id: Int) {
    deleteAlbumAlbum(id: $id) {
      album_id
    }
  }
`

export const FIX_ALBUM_ALBUM = gql`
  mutation fixAlbumAlbum($id: Int) {
    fixAlbumAlbum(id: $id) { id }
  }
`
