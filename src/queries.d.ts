import { Album } from './graphql-types'

export const LATEST_VOLUMES: DocumentNode
export const VOLUME: DocumentNode
export const VOLUMES: DocumentNode
export const ALBUM: DocumentNode
export const ALBUMS: DocumentNode
export const STATS: DocumentNode
export const SCAN: DocumentNode
export const WRITE: DocumentNode
export const ALBUM_UPDATED: DocumentNode
export const ALBUM_REMOVED: DocumentNode
export const VOLUME_FEED: DocumentNode
export const FILE_FEED: DocumentNode
export const FILE_REMOVED: DocumentNode
export const SET_ALBUM_NAME: DocumentNode
export const SET_ALBUM_ARTIST: DocumentNode
export const SET_ALBUM_YEAR: DocumentNode
export const SET_ALBUM_VOLUME: DocumentNode
export const SET_TRACK_ARTIST: DocumentNode
export const SET_TRACK_NAME: DocumentNode

interface QueryAlbum extends Album {
  name: NonNullable<Album['name']>
  thumbnail: NonNullable<Album['thumbnail']>
}

export type AlbumsQuery = QueryAlbum[]
