import { ApolloClient, MutationUpdaterFunction, DefaultContext, ApolloCache, NormalizedCacheObject, Reference } from '@apollo/client'
import { client } from './apollo/client'
import { Album, MutationUpdateAlbumArtistArgs, MutationUpdateAlbumYearArgs, Track } from './graphql-types'
import { ALBUM, SET_ALBUM_NAME, SET_ALBUM_ARTIST, SET_ALBUM_YEAR, SET_ALBUM_VOLUME, SET_TRACK_ARTIST, SET_TRACK_NAME } from './queries'
import gql from 'graphql-tag'

type Cache = {
  data: {
    data: {[id: string]: any};
  }
}

class Store {
  Album = {
    set_name (id: string, name: string, _client:ApolloClient<NormalizedCacheObject> = client) {
      return new Promise(resolve => {
        _client.mutate({
          mutation: SET_ALBUM_NAME,
          variables: {id, name},
          update: (cache: ApolloCache<any>) => {
            cache.readQuery({ query: ALBUM, variables: { id } })
            cache.writeFragment({
              id: 'Album' + id,
              fragment: gql`
                fragment MyAlbum on Album {
                  name
                }
              `,
              data: { name },
            })
            resolve((cache as unknown as Cache).data.data['Album' + id])
          }
        })
      })
    },

    set_artist (album_id: MutationUpdateAlbumArtistArgs['album_id'], artist: string) {
      client.mutate({
        mutation: SET_ALBUM_ARTIST,
        variables: {id: +album_id, artist},
        update: cache => {
          cache.readQuery({ query: ALBUM, variables: { id: album_id } }) // this creates an empty object
          cache.writeFragment({
            id: 'Album' + album_id,
            fragment: gql`
              fragment MyAlbumArtist on Album {
                artist
              }
            `,
            data: { artist },
          })
        }
      })
    },

    async set_year (album_id: string, year: string) {
      await client.mutate({
        mutation: SET_ALBUM_YEAR,
        variables: {id: +album_id, year: year + '-01-01'},
        update: cache => {
          const album: Album & Reference = (cache as unknown as Cache).data.data['Album' + album_id] || (cache as unknown as Cache).data.data.ROOT_QUERY[`albums({"id":${album_id}})`]?.albums[0]
          album.tracks.forEach(track => {
            cache.writeFragment({
              id: (track as unknown as Reference).__ref,
              fragment: gql`
                fragment MyTrack on Track {
                  year
                }
              `,
              data: { year },
            })
          })
        }
      })
    },

    async set_volume (album_id: string, volume: string) {
      await client.mutate({
        mutation: SET_ALBUM_VOLUME,
        variables: {id: +album_id, volume},
      })
    },

    set_track_artist (track_id: string, artist: string) {
      client.mutate({
        mutation: SET_TRACK_ARTIST,
        variables: {id: track_id, artist}
      })
      .then(() => console.log('mutation done'))
    },

    set_track_name (track_id: string, name: string) {
      client.mutate({
        mutation: SET_TRACK_NAME,
        variables: {id: track_id, name}
      })
      .then(() => console.log('mutation done'))
    },

    thumbnail () {
    }
  }
}

export default new Store()
