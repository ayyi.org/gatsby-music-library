declare class Album {
  set_name (id: number, name: string): string
  set_artist (id: number, name: string): string
  set_year (id: number, name: string): string
  set_track_name (id: number, name: string): string
  set_track_artist (id: number, artist: string): string
  set_volume (id: number, volume: string): string
}

declare class MobxStore {
  Album: Album
}

export default new MobxStore()
