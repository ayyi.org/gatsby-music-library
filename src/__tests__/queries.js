import { graphql } from 'graphql'
import { mockServer } from '@graphql-tools/mock'

import { client } from '../../__mocks__/graphql-server'
import typeDefs from '../schema'
import { SEARCH, VOLUMES, ALBUM, ADD_ALBUM, DELETE_ALBUM_ALBUM } from '../queries'
import { Schema } from '../../__mocks__/graphql-server'
import AppStore from '../store'

let schemaWithMocks
let server

beforeAll(() => {
  const preserveResolvers = true

  const mocks = {
    Thumbnail: () => 'thumbnail',
    Mutation: () => ({
      /*
      updateAlbumName: (args, _, context) => {
        // since upgrading 6->7 no longer get args here. currently using other mock
        return {name: args.name}
      }
      */
    })
  }

  server = mockServer(typeDefs, mocks, preserveResolvers)
  schemaWithMocks = Schema
})

describe('Queries', () => {
  it('creates album', async () => {
    const { data } = await client.mutate({
      mutation: ADD_ALBUM,
      variables: {name: 'test album', artist: 'test artist', volume: '9999'}
    })

    expect(typeof data.addAlbum).toEqual('string')
    expect(+data.addAlbum).toBeGreaterThan(1)

    await client.query({
      query: ALBUM,
      variables: {id: +data.addAlbum}
    })

    const { errors, ...del } = await client.mutate({
      mutation: DELETE_ALBUM_ALBUM,
      variables: {id: +data.addAlbum}
    })
    expect(errors).toEqual(undefined)

    expect(del.data.deleteAlbumAlbum.album_id).toEqual(+data.addAlbum)
    expect(del.data.deleteAlbumAlbum.__typename).toEqual('Album')
  })

  it('can search', async () => {
    const { data: { tracks }, errors } = await graphql(schemaWithMocks, SEARCH.loc.source.body, null, null, {search: 'a'})
    expect(errors).toEqual(undefined)

    expect(typeof tracks[0].songnb).toEqual('number')
  })

  it('queries volumes', async () => {
    const { data: { volumes }, errors } = await server.query(VOLUMES.loc.source.body, {first: '1'})
    expect(errors).toEqual(undefined)

    expect(volumes.volumes.length).toBeGreaterThan(0)
    expect(volumes.volumes[0].thumbnail).toContain('thumbnail')
  })
})

describe('Mutations', () => {
  it('sets album name', async () => {
    const new_name = 'New name'

    {
      const { data: { albums }, errors } = await client.query({query: ALBUM, variables: {id: 1}})
      expect(errors).toEqual(undefined)
      var id = albums.albums[0].album_id
    }

    const album = await AppStore.Album.set_name(id, new_name, client)

    expect(album.__typename).toEqual('Album')
    expect(album.name).toEqual(new_name)

    {
      const { data: { albums }, errors } = await client.query({query: ALBUM, variables: { id }})
      expect(errors).toEqual(undefined)

      expect(albums.albums[0].album_id).toEqual(id)
      expect(albums.albums[0].name).toEqual(new_name)
    }
  })
})
