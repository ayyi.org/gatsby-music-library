import React from 'react'
import { graphql, useStaticQuery } from 'gatsby'
import Navbar from './navbar'
import styled from 'styled-components'
import { Helmet } from 'react-helmet'

export interface LayoutProps {
  children: any
  uri: string
}

const Page = styled.div`
  display: grid;
  grid-template-columns: 60px;
  @media (max-width: 768px) {
    grid-template-columns: 0px;
  }
`
const Left = styled.div`
  grid-column: 1;
  padding: 0;
`
const Right = styled.main`
  grid-column: 2;
  padding: 0 20px
`

                          //declare const Layout: React.SFC<LayoutProps>
// TODO can combine left and navbar
const Layout = ({ children, uri }: LayoutProps) => {
  const page = uri?.split('/')[1] || 'home'

  const data = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            darkMode
          }
        }
      }
    `
  )

  return <>
    <Helmet title={data.site.siteMetadata.title} htmlAttributes={{ lang: 'en' }} bodyAttributes={{ class: `font-sans ${page}-page ${data.site.siteMetadata.darkMode ? 'dark' : ''}` }} />
    <Navbar/>
    <Page>
      <Left className='layout-left'></Left>
      <Right>{children}</Right>
    </Page>
  </>
}

export default Layout
