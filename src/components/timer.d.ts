import * as React from "react"

export interface VolData {
  example?: boolean
}

export interface TimerProps {
  data: VolData
}

declare const Timer: React.SFC<TimerProps>

export default Timer
