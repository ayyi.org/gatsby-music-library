import React from 'react'
import PropTypes from 'prop-types'

const Timer = ({data}) => {
  const [start_time, set_start_time] = React.useState('')

  React.useEffect(() => {
    set_start_time(new Date())
  }, [])

  return <div id='timer'>
    {data && <p>Query time: {((new Date() - start_time)/1000).toFixed(2)}</p>}
    </div>
}

Timer.propTypes = {
  data: PropTypes.object
}

export default Timer
