/*
 *  Uncomment below to re-enable the albums page
 */
import React from 'react'
import { Link } from 'gatsby'
import styled from 'styled-components'
import icon1 from '../../images/icons/go-home.svg'
import icon2 from '../../images/icons/document-preview.svg'
import icon3 from '../../images/icons/diag_class.svg'
import icon4 from '../../images/icons/media-optical.svg'
import icon5 from '../../images/icons/flash.svg'
import icon6 from '../../images/icons/dblatex.svg'

const NavItem = styled(Link)`
  text-decoration: none;
  display: inline-block;
  margin: 0 0.75vw 0 0;
  padding: 8px 14px;
  color: #000;
`

const NavbarLinks = () => {
  return (
    <>
      <NavItem to="/" className="home"><img src={icon1} alt="Home"/></NavItem>
      <NavItem to="/search" className="search"><img src={icon2} alt="Search"/></NavItem>
      <NavItem to="/albums" className="albums"><img src={icon3} alt="Albums"/></NavItem>
      <NavItem to="/volumes" className="volumes"><img src={icon4} alt="Volumes"/></NavItem>
      <NavItem to="/scan" className="scan"><img src={icon5} alt="Scan" title="Scan"/></NavItem>
      <NavItem to="/admin" className="admin"><img src={icon6} alt="Admin"/></NavItem>
    </>
  )
}

export default NavbarLinks
