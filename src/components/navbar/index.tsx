import React from 'react'
import Links from './links'
import styled from 'styled-components'

const Navigation = styled.nav`
  width: 60px;
  min-height: 100%;
  @media (max-width: 768px) {
    width: 100%;
    height: 51px;
    min-height: 50px;
    z-index: 100;
  }
`

const Navbar = () => {
  return (
    <Navigation className="navbar fixed bg-gray-100 pt-0 md:pt-3 box-border">
      <Links />
    </Navigation>
  )
}

export default Navbar
