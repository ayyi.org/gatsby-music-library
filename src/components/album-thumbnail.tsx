import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'
import styled from 'styled-components'
import { CSSTransition } from 'react-transition-group'
import config from '../../.config.json'
import './volume-thumbnail.sass'

const Volume = styled.li`
  width: 110px;
  height: 110px;
`

export type AlbumThumbnailProps = {
  album_id?: number,
  id: string,
  name: string,
  thumbnail: string
  link: string
  delay: number
}

let i = -1

const Component = ({ id, name, thumbnail, link, delay }: AlbumThumbnailProps) => {
  i++

  return <Volume className="float-left relative mr-4 mb-4">
    <Link to={`${link}${id}`} className="text-gray-000">
      <CSSTransition in={true} timeout={1000} appear classNames="my-fade">
        <div style={{transitionDelay: (delay + 250 * i) + 'ms'}}>
          <div className="absolute text-xs bg-gray-999 bg-opacity-50 px-0.75 py-0.5 bottom-0 left-0">{name}</div>
          <img src={`${config.artwork}/${thumbnail}?width=110`} alt={thumbnail}/>
        </div>
      </CSSTransition>
    </Link>
  </Volume>
}

export default Component
