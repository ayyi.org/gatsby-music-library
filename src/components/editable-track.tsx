/*
 *  Single line with editable artist and track-name
 */
import React from 'react'
import styled from 'styled-components'
import { EditText } from 'react-edit-text'
import Store from '../store'
import { Track as T } from '../model'

const Time1 = styled.span`
`
const Time2 = styled.span`
`

const Track = (t: T) =>
  <React.Fragment key={t.id}>
    <span className="songnb">{t.songnb}</span>
    <EditText
      className="artist"
      defaultValue={t.artist}
      onSave={({ value }) => {
        if(value !== t.artist){
          Store.Album.set_track_artist(t.id as unknown as string, value)
        }
      }}
    />
    <EditText
      className="name"
      defaultValue={t.name}
      onSave={({ value }) => {
        if(value !== t.name){
          Store.Album.set_track_name(t.id as unknown as string, value)
        }
      }}
    />
    <Time1>{t.min}</Time1><span>:</span><Time2>{('0' + t.sec).slice(0, 2)}</Time2>
</React.Fragment>

export default Track
