import * as React from "react"

export interface ChartsProps {
  example?: boolean;
}

declare const Charts: React.SFC<ChartsProps>

export default Charts
