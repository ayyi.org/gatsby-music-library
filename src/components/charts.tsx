import React, { useRef, useEffect, MutableRefObject } from 'react'
import * as d3 from 'd3'
import { useQuery } from '@apollo/client'
import { STATS } from '../queries'
import { Genre } from '../graphql-types'

const margin = 60
const width = 588 - 2 * margin
const height = 400 - 2 * margin

const draw = (ref: MutableRefObject<SVGSVGElement>, data: Genre[]) => {
  const svg = d3.select(ref.current)

  const yScale = d3.scaleLinear()
    .range([height, 0])
    .domain([0, 18000])

  const chart = svg.append('g')
    .attr('transform', `translate(${margin}, ${margin})`)

  chart.append('g')
    .call(d3.axisLeft(yScale))

  const xScale = d3.scaleBand()
    .range([0, width])
    .domain(data.map(d => d.name))
    .padding(0.2)

  chart.append('g')
    .attr('transform', `translate(0, ${height})`)
    .call(d3.axisBottom(xScale))

  const barGroups = chart.selectAll()
    .data(data)
    .enter()
    .append('g')

  barGroups
    .append('rect')
    .attr('class', 'bar')
    .attr('x', g => xScale(g.name) as number)
    .attr('y', g => yScale(g.count))
    .attr('height', g => height - yScale(g.count))
    .attr('width', xScale.bandwidth())
}

const Charts = () => {
  const ref = useRef<SVGSVGElement>()
  const { data } = useQuery(STATS)

  useEffect(() => {
    data && draw(ref as MutableRefObject<SVGSVGElement>, data.stats.genres)
  }, [data])

  return <>
    <svg id="chart-1" width={width + 2 * margin} height={height + 2 * margin} ref={ref as MutableRefObject<SVGSVGElement>} style={{border: 'solid 1px #888'}}></svg>
  </>
}

export default Charts
