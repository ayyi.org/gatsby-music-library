import React from 'react'
import { Link } from 'gatsby'
import { useQuery } from '@apollo/client'
import { VOLUME } from '../queries'
import Album, { AlbumData } from './album'

type VolumeProps = {
  id: string
}

type Volume = AlbumData[]

const VolumePage: React.FC<VolumeProps> = ({ id }) => {
  const { data } = useQuery(VOLUME, {variables: {id}})

  const albums = (volume: Volume) =>
    <>
      <p>Volume {id} contains {volume.length} albums</p>
      <ul>{volume.map(v =>
        <li key={v.album_id}>
          <Album name={v.name} tracks={v.tracks} artist={v.artist} collapsed={volume.length > 2}/>
        </li>)}
      </ul>
    </>

  return <div>
    <h2 className="mt-0"><Link to={`/volumes/${id}`}>Volume {id}</Link></h2>
    {data && albums(data.volumes.volumes[0].albums)}
  </div>
}

export default VolumePage
