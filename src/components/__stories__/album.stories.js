import React from 'react'
import Album from '../album'
import { store } from '../../../__mocks__/graphql-server'

export default {
  title: 'Album',
}

const tracks = [store.store.Track['1'], store.store.Track['2']]

export const exampleStory = () => (
  <div>
    <h1>Album component</h1>
    <Album id={1} name='Album 1' tracks={tracks}/>
  </div>
)
