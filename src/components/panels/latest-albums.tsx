import React from 'react'
import { useLazyQuery } from '@apollo/client'
import { ALBUMS, AlbumsQuery } from '../../queries'
import AlbumThumbnail from '../album-thumbnail'

const LatestAlbums = ({ delay }: {delay: number}) => {
  const [getData, { data }] = useLazyQuery(
    ALBUMS,
    {
      variables: {limit: 5},
      onCompleted: () => {
      }
    }
  )

  React.useEffect(() => {
      data || getData()
    },
    [data]
  )

  const albums: AlbumsQuery = data?.albums.albums

  const show_albums = () => <>
    {albums.map(v =>
      <AlbumThumbnail
        key={v.album_id}
        {...v}
        id={'' + v.album_id}
        link={'/albums/'}
        delay={delay}
      />
    )}
  </>

  return <>
    <ul className="flow-root" style={{minHeight: 120}}>
      {albums && show_albums()}
    </ul>
  </>
}

export default LatestAlbums
