import React from 'react'
import { useLazyQuery } from '@apollo/client'
import { LATEST_VOLUMES } from '../../queries'
import Timer from '../timer'
import VolumeThumbnail, { VolumeThumbnailType } from '../volume-thumbnail'

const LatestVolumes = ({ delay }: {delay: number}) => {
  const [getData, { error, data }] = useLazyQuery(
    LATEST_VOLUMES,
    {onCompleted: () => {
    }}
  )

  React.useEffect(() => {
    data || (
      getData()
    )},
    [data]
  )

  const volumes = data?.latestVolumes

  const show_volumes = () => <>
    {volumes.map((v: VolumeThumbnailType) =>
      <VolumeThumbnail
        key={v.id}
        props={{...v, link: '/volumes/', delay}}
      />
    )}
  </>

  return <>
    <Timer data={data} />
    {error && <p>{'' + error}</p>}
    <ul className="flow-root" style={{minHeight: 120}}>
      {volumes && show_volumes()}
    </ul>
  </>
}

export default LatestVolumes
