import React from 'react'
import LatestVolumes from './latest-volumes'
import LatestAlbums from './latest-albums'

interface ComponentOptions {
   [key: string]: React.FunctionComponent<any>
}

const Components: ComponentOptions = {
  'Latest volumes': LatestVolumes,
  'Latest albums': LatestAlbums,
}

const Panels = ({ types }: {types: string[]}) => {
  return <>
    {types.map((type, i) => <section key={type} id={type.toLowerCase().replace(' ', '-')}>
      <h2>{type}</h2>
      {React.createElement(Components[type], {delay: i * 1000})}
    </section>
    )}
  </>
}

export default Panels
