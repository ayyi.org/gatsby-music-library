import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import config from '../../.config'

String.prototype.translate = function(translation) {
    const names = []
    for (const search in translation) {
        if (translation.hasOwnProperty(search)) {
            names.push(search.replace(/([.\\+*?[^\]$(){}=!<>|:-])/g, "\\$1"))
        }
    }
    var re = new RegExp("(?:" + names.join("|") + ")", "g")
    return this.replace(re, val => translation[val])
}

const to_filename = s => {
  if(!s) return ''
											/*
											console.log('translating:', s
		.split('')
		.map(function (char) {
			return '' + char + '(' + char.charCodeAt(0) + ')';
		})
		.reduce(function (previous, current) {
														//console.log(current, String.fromCharCode(current))
			//return '' + previous + ' ' + current + ' (' + String.fromCharCode(current) + ')';
			return '' + previous + ' ' + current;
		})
	)
											*/

	const translation = {
		" ": "_",
		".": "",
		",": "",
		"!": "",
		"/": "",
		"&amp;": "",
		"&": "",
		"#": "",
		";": "",
		":": "",
		'"': "",
		'%': "",
		"&#233;": "e",
		// japanese utf-8 - see http://utf8-chartable.de/unicode-utf8-table.pl
		"%E3%81%95": "_uE38195",
		"%E3%82%93": "_uE38293",
		"?": "_uE38293",
	}
	s = s.translate(translation)
	return s.toLowerCase()
}

const Thumbnail = styled.img`
  width: 110px;
  height: 110px;
  overflow: hidden;
  display: block;
`

const thumb = (artist, name) => `${config.artwork}/${to_filename(artist)}-${to_filename(name)}.jpg`

const Component = ({artist, name, size}) => {
  const style = size ? {width: size, height: size} : {}
  return <Thumbnail src={thumb(artist, name)} alt="Album thumbnail" style={style}/>
}

Component.propTypes = {
  artist: PropTypes.string,
  name: PropTypes.string,
  size: PropTypes.number
}

export default Component
