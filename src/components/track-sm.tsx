import React from 'react'
import styled from 'styled-components'
import Store from '../store'
import { Track as T } from '../model'

const Time1 = styled.span`
`
const Time2 = styled.span`
`

const Track = (t: T) =>
  <React.Fragment key={t.id}>
    <span className="songnb">{t.songnb}</span>
    <div className="col-2">
      <div>{t.artist}</div>
      <div>{t.name}</div>
    </div>
    <Time1>{t.min}</Time1><span>:</span><Time2>{('0' + t.sec).slice(0, 2)}</Time2>
</React.Fragment>

export default Track
