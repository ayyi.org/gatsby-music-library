import React from 'react'
import { create } from 'react-test-renderer'
import Album from '../album'
import { store } from '../../../__mocks__/graphql-server'

const tracks = [store.store.Track['1'], store.store.Track['2']]

describe('Album component', function() {
  it('has title', () => {
    const tree = create(<Album id={1} name='Album 1' tracks={tracks}/>)
    const heading = tree.root.findByType('h3')
    heading.find(v => v.props.children === 'Album 1')
  })

  it('various album has no artist', () => {
    const tree = create(<Album id={1} name='Album 1' tracks={tracks}/>)
    const heading = tree.root.findByType('h3')
    expect(heading.findAll(v => v.props.children === 'Artist 1')).toHaveLength(0)
  })

  it('has volume', () => {
    const tree = create(<Album id={1} name='Album 1' tracks={tracks}/>)
    const heading = tree.root.findByType('h3')
    heading.find(v => v.props.children === '001')
  })

  it('has tracks', () => {
    const tree = create(<Album id={1} name='Album 1' tracks={tracks}/>)
    const list = tree.root.findByType('ul')

    expect(list.children).toHaveLength(2)
    expect(list.findAll(v => v.props.children === 'va')).toHaveLength(4)
  })
})
