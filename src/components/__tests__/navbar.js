import React from 'react'
import { create } from 'react-test-renderer'
import Navbar from '../navbar'

describe('Navbar', () => {
  it('renders correctly', () => {
    const tree = create(<Navbar/>)
      .toJSON()

    expect(tree).toMatchSnapshot()
  })
})
