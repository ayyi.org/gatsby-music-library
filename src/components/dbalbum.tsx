import React from 'react'
import { RouteComponentProps } from '@reach/router'
import { Link } from 'gatsby'
import styled from 'styled-components'
import CTE from 'react-click-to-edit'
import useQuery from 'hooks/useTimedQuery'
import { ALBUM } from 'queries'
import Store from 'store'
import { Track as T } from 'model'
import Track from './editable-track'
import TrackMD from './track-md'
import TrackSM from './track-sm'
import Thumbnail from './thumbnail'
import { currentBreakpoint } from 'utils/breakpoints'
import useMedia from 'use-media'

const EditableDL = styled.dl`
  dd {
    margin-left: -3px;
  }
`
const Label = styled.dt`
  font-size: 12px;
  margin-top: 2px;
`
const Value = styled.dd`
  font-weight: bold;
  padding: 4px 0 12px;
  margin-left: 0px;
`

type Params = {
  id: string
}

interface TrackComponentOptions {
   [key: string]: (t: T) => JSX.Element
}

const components: TrackComponentOptions = {
  md: TrackMD,
  sm: TrackSM,
}

const Album: React.FC<RouteComponentProps<Params>> = props => {
  const { loading, error, data, time } = useQuery(ALBUM, {id: +(props.id as string)})

  useMedia({minWidth: '640px'})
  useMedia({minWidth: '768px'})
  useMedia({minWidth: '1024px'})

  const d = data?.albums?.albums[0]

  const on_album_name_edit = (val: string) => {
    if(val !== d.name){
      Store.Album.set_name(props.id as string, val)
    }
  }

  const on_album_artist_edit = (val: string) => {
    if(val !== d.artist){
      Store.Album.set_artist(+(props.id as string), val)
    }
  }

  const on_volume_edit = (val: string) => {
    if(val !== d.tracks[0].volume){
      Store.Album.set_volume(props.id as string, val)
    }
  }

  const on_year_edit = async (val: string) => {
    if(val !== d.tracks[0].year && val.trim().length === 4){
      Store.Album.set_year(props.id as string, val)
    }
  }

  const album = () =>
    <div className='album-layout'>
      <div className='album-inspector' style={{gridColumn: '1'}}>
        <div style={{marginBottom: 20}}>
          <Thumbnail artist={d.artist} name={d.name} size={220}/>
        </div>
        {
          [
            ['Album name', d.name, on_album_name_edit],
            ['Artist', d.artist || '', on_album_artist_edit],
            ['Volume', d.tracks[0].volume, on_volume_edit],
            ['Date', d.tracks[0].year || 'No date', on_year_edit]
          ]
          .map(item => <EditableDL key={item[0]}>
            <Label>
              {item[0]}
            </Label>
            <Value id={item[0].toLowerCase().replace(' ', '-') + '-edit'} className="hover:bg">
              <CTE initialValue={item[1]} endEditing={item[2]} inputClass='cte-input'/>
            </Value>
          </EditableDL>
        )}

        {/* non-editable fields */}
        <dl>
          <Label>
            Format
          </Label>
          <Value>
            {d.format}
          </Value>
        </dl>
        <p>Query took {((time || 0) / 1000).toFixed(2)}s</p>
      </div>

      <div className="album-main">
        <div className="track-list">{d.tracks.map(components[currentBreakpoint()] || Track)}</div>
      </div>
    </div>

  // ⬅
  return <>
    <nav className='top' aria-label='section-nav'><Link to="/albums">&lt; Albums</Link></nav>
    <h1>{d && d.artist !== 'va' ? `${d.artist} - ` : ''} {d ? d.name : props.id}</h1>
    {d && album()}
  </>
}

export default Album
