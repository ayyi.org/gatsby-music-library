import React from 'react'
import { RouteComponentProps } from '@reach/router'
import { Link } from 'gatsby'
import styled from 'styled-components'
import Thumbnail from './thumbnail'
import { Track } from '../model'
import { Song, Name, Names, Artist, Left, Right, HeadAlbum, HeadVolume, albumStyles } from './album.styles'

export interface AlbumData {
  album_id: number
  name: string
  artist: string
  tracks: Track[]
}

export type AlbumProps = RouteComponentProps & {
  name: string
  tracks: any
  artist: string
  collapsed?: boolean
}

const Nb = styled.span<{ wide: boolean }>`
  min-width: ${props => props.wide ? 25 : 15}px;
`
const Time1 = styled.span`
  min-width: 1.25em;
  text-align: right;
`
const Time2 = styled.span``
const AlbumHead = styled.h3`
  margin-bottom: 8px;
  line-height: 20px;
`
const HeadYear = styled.span`
  padding: 0 0.3em;
  margin-right: 0.3em;
`
const More = styled.div`
  position: absolute;
  bottom: 3px;
  right: 8px;
  font-size: 18px;
`

const album_head = (name: string, artist: string, id: number, year: string, volume: string) =>
  <>
    {artist.toLowerCase() === 'va' ? '' : <span className="album-head-artist">{artist}</span>}
    <Link to={`/albums/${id}`}>
      <HeadAlbum css={[]}>{name}</HeadAlbum>
    </Link>
    {year && <HeadYear className="year">{year?.slice(0, 4)}</HeadYear>}
    <HeadVolume>{volume}</HeadVolume>
  </>

const AlbumComponent = ({name, tracks, artist, collapsed = true}: AlbumProps) => {

  const [expanded, setExpanded] = React.useState(!collapsed)
  const album = tracks[0].albumObj
  const col1 = +tracks[tracks.length - 1].songnb > 99

  artist = artist || album?.artist || tracks.reduce(
    (acc: string, v: Track) => (v.artist !== acc || v.artist?.toLowerCase() === 'various') ? 'va' : acc,
    tracks[0].artist || ''
  )

  const track = (t: Track) =>
    <Song className='text-sm' key={t.id}>
      <Nb wide={col1}>{t.songnb ? t.songnb : ''}</Nb>
      <Names>
        {artist === 'va' ? <Artist>{t.artist}</Artist> : ''}
        <Name>{t.name}</Name>
      </Names>
      <Time1>{t.min}</Time1>:<Time2>{('0' + t.sec).slice(0, 2)}</Time2>
    </Song>

  const more = () => setExpanded(!expanded)

  const n_rows = expanded ? 100 : (name.length + artist.length) < 35 ? 6 : 4

  return (<div className={'card' + (expanded ? ' expanded' : '')} css={[albumStyles]}>
    <Left>
      <Thumbnail artist={artist} name={name}/>
    </Left>
    <Right>
      <AlbumHead>{album_head(name, artist, album?.album_id, tracks[0].year, tracks[0].volume)}</AlbumHead>
      <ul>
        {tracks.filter((_a: Track, i: number) => i < n_rows).map(track)}
      </ul>
      {tracks.length > n_rows &&
        <More>
          <a onClick={more}>⌄</a>
        </More>
      }
      {expanded &&
        <More>
          <a onClick={more}>^</a>
        </More>
      }
    </Right>
  </div>)
}

export default AlbumComponent
