import * as React from 'react'

export interface ThumbnailProps {
  children?: ReactNode
  name: string
  artist: string
  size?: number
}

declare const Thumbnail: React.SFC<ThumbnailProps>

export default Thumbnail
