import React from 'react'
import { Link } from 'gatsby'
import styled from 'styled-components'
import { CSSTransition } from 'react-transition-group'
import config from '../../.config.json'
import { Volume } from '../graphql-types'
import './volume-thumbnail.sass'

const Li = styled.li`
  width: 110px;
  height: 110px;
`

export type VolumeThumbnailType = Volume & {
  link?: string
  delay?: number
}

export type VolumeThumbnailProps = {
  props: VolumeThumbnailType
}

let i = -1

const Component = ({ props: { id, thumbnail, link } }: VolumeThumbnailProps) => {
  i++

  return <Li className="float-left relative mr-4 mb-4">
    <Link to={`${link||''}${id}`} className="text-gray-000">
      <CSSTransition in={true} timeout={1000} appear classNames="my-fade">
        <div style={{transitionDelay: 250 * i + 'ms'}}>
          <div className="absolute text-xs bg-gray-999 bg-opacity-50 px-0.75 py-0.5 bottom-0 left-0">{id}</div>
          <img src={`${config.artwork}/${thumbnail}`} alt={thumbnail}/>
        </div>
      </CSSTransition>
    </Link>
  </Li>
}

export default Component
