import tw, { css } from 'twin.macro'

const Song = tw.li`flex flex-nowrap leading-14px`
const Names = tw.span`flex flex-nowrap max-w-xl w-390 xl:w-330 2xl:w-390 py-0 px-4`
const Artist = tw.span`flex flex-nowrap pr-2 text-yellow-200 max-w-lg whitespace-nowrap overflow-ellipsis overflow-hidden`
const Name = tw.span`flex flex-nowrap whitespace-nowrap overflow-ellipsis overflow-hidden`
const Left = tw.div`col-start-1`
const Right = tw.div`col-start-2`
const HeadAlbum = tw.span`text-yellow-300 pr-2`
const HeadVolume = tw.span`text-green-300`

const albumStyles = css`
  width: 600px;
  overflow: hidden;
  grid-template-columns: 130px 600px;
  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;
  ${tw`grid relative mr-5 mb-5 p-3`}
  @media (min-width: 1280px) and (max-width: 1535px) {
    width: 550px;
  }
`

export { Song, Names, Artist, Name, Left, Right, HeadAlbum, HeadVolume, albumStyles }
