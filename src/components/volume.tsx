import React from 'react'
import { RouteComponentProps } from '@reach/router'
import { Link } from 'gatsby'
import useQuery from '../hooks/useTimedQuery'
import { VOLUME } from '../queries'
import Album, { AlbumData } from './album'

type Params = {
  id: string
}

type Volume = AlbumData[]

const VolumePage: React.FC<RouteComponentProps<Params>> = ({ id }) => {
  const { data, time } = useQuery(VOLUME, {id: id as string})

  const albums = (volume: Volume) => {
    return <>
      <Link className="btn btn-blue" to={`/volumes/${(+(id as string) - 1).toString().padStart(3, '0')}`}>&lt;</Link>
      <Link className="btn btn-blue" to={`/volumes/${(+(id as string) + 1).toString().padStart(3, '0')}`}>&gt;</Link>
      <p>Volume {id} contains {volume.length} albums</p>
      <ul className="album-container xl:grid xl:grid-cols-2">{volume.map(v => <li key={v.album_id}><Album name={v.name} tracks={v.tracks} artist={v.artist} /></li>)}</ul>
      <p>Query took {time && (+time / 1000)/*.toFixed(2)*/}s</p>
    </>
  }

  return <>
    <h1>Volume {id}</h1>
    {data && albums(data.volumes.volumes[0].albums)}
  </>
}

export default VolumePage
