import { AlbumData } from './components/album'

export interface Track {
  name: string
  id: number
  songnb: number
  artist: string
  year: string
  volume: string
  min: number
  sec: number
  albumObj: any
}

export type Volume = AlbumData[]
