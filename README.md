gatsby-music-library
--------------------

![Test Status](https://gitlab.com/ayyi.org/gatsby-music-library/badges/master/pipeline.svg)
![coverage](https://gitlab.com/ayyi.org/gatsby-music-library/badges/master/coverage.svg)

Gatsby Music Library is a simple Gatsby application that acts as a demonstration
front-end for the [music-db-graphql](https://gitlab.com/ayyi.org/music-db-graphql) server.

![Screenshot](screenshot.jpg "Screenshot")

The project is fully linted, has unit tests with Jest and React Testing
Library, and has end-to-end tests using Cypress.

```
npm run lint
npm run test
npm run test:e2e
```

[Tailwindcss](https://tailwindcss.com/) is used for styling, to provide
high-level but performant management of the CSS.

The layout is responsive, with layouts for 640, 768, 1024, 1280, and 1536 pixel
devices.

There is a Scan and Import facility to add to the database from a
filesystem. GraphQL subscriptions are used to make make this process interactive.

Metadata editing is supported in detail views using an in-place editor. Clicking on a field
such as Artist or Track Name starts the editor and allows the names to be changed.
