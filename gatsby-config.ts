import type { GatsbyConfig } from 'gatsby'
import resolveConfig from 'tailwindcss/resolveConfig'
import tailwindConfig from './tailwind.config'

require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`,
})

const fs = require('fs')
const yaml = require('js-yaml')

const config_dir = process.env.NODE_ENV === 'test' || process.env.CYPRESS_E2E === '1' ? '__mocks__/' : './'

const _config = yaml.load(
  fs.readFileSync(
    fs.existsSync(config_dir + 'config.yaml')
      ? config_dir + 'config.yaml'
      : './__mocks__/config.yaml',
    'utf8'
  )
)
fs.writeFileSync('./.config.json', JSON.stringify(_config), 'utf8')

fs.writeFileSync('./.screens.json', JSON.stringify(resolveConfig(tailwindConfig).theme.screens), 'utf8')

const config: GatsbyConfig = {
  siteMetadata: {
    title: `mp3-gatsby`,
    siteUrl: `https://www.yourdomain.tld`,
    darkMode: true
  },

  //
  // More easily incorporate content into your pages through automatic TypeScript type generation and better GraphQL IntelliSense.
  // https://gatsby.dev/graphql-typegen
  //
  graphqlTypegen: true,

  plugins: [
    {
      resolve: 'gatsby-plugin-styled-components',
    },
    {
      resolve: 'gatsby-plugin-sass',
      options: {
        implementation: require('sass'),
        postCssPlugins: [
          require('tailwindcss'),
          require('./tailwind.config.js') // Optional: Load custom Tailwind CSS configuration
        ]
      }
    },

    //  unminify html for production builds
    'gatsby-plugin-prettier-build',

    {
      resolve: 'gatsby-plugin-webpack-bundle-analyser-v2',
      options: {
        openAnalyzer: false,
        analyzerMode: 'static'
      }
    },

    /*  enable this to generate the favicon, `npm run build`
     *      then: cp -p public/favicons/favicon.ico static/
     *      for it to work with dev server
     *  -it just calls favicons-webpack-plugin, which calls favicons (https://github.com/itgalaxy/favicons)
    {
      resolve: 'gatsby-plugin-favicons',
      options: {
        logo: './src/images/portfolio-ico.png'
      }
    }
    */
  ]
};

export default config;
