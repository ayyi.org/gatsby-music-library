describe('Albums page', () => {

  before(() => {
    cy.visit('/albums').get('#gatsby-focus-wrapper').injectAxe()
  })

  it('Index page is ok for ally', () => {
    cy.checkA11y()
  })

  it('Navigates to detail page', () => {
    cy.get('#albums a.album:first').click()
    cy.location('pathname').should('include', '/albums/')
  })

  it('Detail page is ok for ally', () => {
    cy.checkA11y()
  })

  it('Has thumbnail', () => {
    cy.get('.album-inspector img')
      .should('be.visible')
      .and(($img) => {
        expect($img[0].naturalWidth).to.be.greaterThan(0)
      })
  })

  it('Has name', () => {
    cy.get('#album-name-edit').invoke('text').should('not.be.empty')
  })
})
