describe('Volumes page', () => {

  before(() => {
    cy.visit('/volumes').get('#gatsby-focus-wrapper').injectAxe()
  })

  it('Is ok for ally', () => {
    cy.checkA11y()
  })
})
