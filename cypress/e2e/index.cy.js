/// <reference types="Cypress" />

describe('Homepage', () => {

  beforeEach(() => {
    cy.visit('/').get('#gatsby-focus-wrapper').injectAxe()
  })

  it('Has no detectable accessibility violations on load', () => {
    cy.checkA11y()
  })

  it('Has thumbnail', () => {
    cy.get('#latest-volumes > ul > li img')
      .should('be.visible')
      .and(($img) => {
        expect($img[0].naturalWidth).to.be.greaterThan(0)
      })
  })
})
