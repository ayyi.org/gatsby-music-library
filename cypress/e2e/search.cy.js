describe('Search page', () => {

  before(() => {
    cy.visit('/search').get('#gatsby-focus-wrapper').injectAxe()
  })

  it('Has search box', () => {
    cy.get('form').within(() => {
      cy.get('input').should('be.visible')
    })
  })

  it('Submits search', () => {
    cy.get('[placeholder="Search"]')
      .should('be.visible')
      .type('test')

    cy.get('form').submit()

    cy.get('main > ul > li').its('length').should('be.gt', 0)
  })

  it('Is ok for ally', () => {
    cy.checkA11y()
  })
})
