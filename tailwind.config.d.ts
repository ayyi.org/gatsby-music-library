declare class TailwindConfig {
  theme: any
  corePlugins: any
  plugins: any
  purge: any
  presets: any
  darkMode: bool
  variantOrder: any
  prefix: any
  important: any
  separator: any
}

export default new TailwindConfig()
