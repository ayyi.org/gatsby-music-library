module.exports = {
  transform: {
    "^.+\\.tsx?$": 'ts-jest',
    "^.+\\.jsx?$": `<rootDir>/jest-preprocess.js`,
  },
  moduleNameMapper: {
    ".+\\.(css|styl|less|sass|scss)$": `identity-obj-proxy`,
    ".+\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": `<rootDir>/__mocks__/file-mock.js`,
    "^@reach/router(.*)": "<rootDir>/node_modules/@gatsbyjs/reach-router$1",
    'd3': '<rootDir>/node_modules/d3/dist/d3.min.js',
  },
  modulePaths: ['<rootDir>/src/'],
  testPathIgnorePatterns: [`node_modules`, 'cache', `\\.cache`, `<rootDir>.*/public`, 'cypress'],
  transformIgnorePatterns: [`node_modules/(?!(gatsby)/)`],
  globals: {
    __PATH_PREFIX__: ``,
  },
  testEnvironmentOptions: {
    url: `http://localhost`,
  },
  setupFiles: [`<rootDir>/loadershim.js`],
  globalSetup: '<rootDir>/__mocks__/setup.js'
}
