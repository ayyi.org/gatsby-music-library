import React from 'react'
import { CSSTransitionProps } from 'react-transition-group/CSSTransition'

export function CSSTransition({ children }: CSSTransitionProps) {
  return <div>
    {children as React.ReactNode}
  </div>
}
