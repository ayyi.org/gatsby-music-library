const fs = require('fs')
const https = require('https')
const yaml = require('js-yaml')
import resolveConfig from 'tailwindcss/resolveConfig'
import tailwindConfig from '../tailwind.config'

module.exports = () => {
  return Promise.all(
    [
      // fetch schema

      new Promise((resolve, reject) => {
        if (!fs.existsSync('./src/schema.js')) {
          const file = fs.createWriteStream('src/schema.js')

          https.get('https://gitlab.com/ayyi.org/music-db-graphql/-/raw/master/schema.js', res => {

            res
              .on('data', data => {
                file.write(Buffer.from(data.toString().replace('apollo-server', 'graphql-tag'), "utf-8"))
              })
              .on('end', () => {
                file.end()
              })

            file.on('finish', () => {
              if (res.statusCode == 404 || res.statusCode == 500) {
                console.log('statusCode:', res.statusCode)
                console.log('headers:', res.headers)
                reject(res)
              } else {
                console.log('\u001b[38;5;240m' + 'downloaded' + '\u001b[0;39m')
                resolve()
              }
            })
          }).on('error', e => {
            console.error(e)
          })
        } else {
          resolve()
        }
      })
    ],
    [
      // create config file

      new Promise((resolve, reject) => {
        const config = yaml.load(fs.readFileSync('./__mocks__/config.yaml', 'utf8'))
        fs.writeFileSync('./.config.json', JSON.stringify(config), 'utf8')

        fs.writeFileSync('./.screens.json', JSON.stringify(resolveConfig(tailwindConfig).theme.screens), 'utf8')

        resolve()
      })
    ]
  )
}
