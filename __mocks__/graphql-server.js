import { makeExecutableSchema } from '@graphql-tools/schema'
import { addMocksToSchema, createMockStore } from '@graphql-tools/mock'
import { ApolloClient } from '@apollo/client'
import { InMemoryCache, defaultDataIdFromObject } from '@apollo/client/cache'
import { SchemaLink } from '@apollo/client/link/schema'

import typeDefs from '../src/schema'

// Make a GraphQL schema with no resolvers
let schema = makeExecutableSchema({ typeDefs })

/*
 *  This store represents server-side data. It is completely separate from the client-side cache
 */
export const store = createMockStore({ schema })

store.set('Track', 1, {id: 1, name: 'Track 1', artist: 'va', songnb: 1, min: '3', sec: '00', volume: '001', albumObj: {album_id: 1}, album: 'Album 1'})
store.set('Track', 2, {id: 2, name: 'Track 2', artist: 'va', songnb: 2, min: '3', sec: '00', volume: '002', albumObj: {album_id: 1}, album: 'Album 1'})
store.set('Track', 3, {id: 3, name: 'Track 3', artist: 'va', songnb: 3, min: '3', sec: '00', albumObj: {album_id: 1}, album: 'Album 1'})
store.set('Track', 4, {id: 4, name: 'Track 4', artist: 'va', songnb: 4, min: '3', sec: '00', albumObj: {album_id: 1}, album: 'Album 1'})
store.set('Track', 5, {id: 5, name: 'Track 5', artist: 'va', songnb: 5, min: '3', sec: '00', albumObj: {album_id: 1}, album: 'Album 1'})
store.set('Track', 6, {id: 6, name: 'Track 6', artist: 'va', songnb: 6, min: '3', sec: '00', albumObj: {album_id: 1}, album: 'Album 1'})
store.set('Track', 7, {id: 7, name: 'Track 7', artist: 'va', songnb: 7, min: '3', sec: '00', albumObj: {album_id: 1}, album: 'Album 1'})

store.set('Track', 8, {id: 8, name: 'Track 1', artist: 'va', songnb: 1, min: '3', sec: '00', albumObj: {album_id: 2}, album: 'Album 2'})

store.set('Album', 1, {album_id: 1, name: 'Album 1', artist: 'Artist 1', tracks: [store.store['Track'][1]], thumbnail: 'thumbnail1'})
store.set('Album', 2, {album_id: 2, name: 'Album 2', artist: 'Artist 2', tracks: [], thumbnail: 'thumbnail2'})

store.set('Volume', '0001', {id: '0001', thumbnail: 'thumbnail', albums: [store.store['Album'][1]]})
store.set('Volume', '0002', {id: '0002', thumbnail: 'thumbnail', albums: []})
store.set('Volume', '0003', {id: '0003', thumbnail: 'thumbnail'})
store.set('Volume', '0004', {id: '0004', thumbnail: 'thumbnail'})
store.set('Volume', '0005', {id: '0005', thumbnail: 'thumbnail'})

schema = addMocksToSchema({
  schema,
  store,
  resolvers: {
    Query: {
      tracks: () => {
        const t = store.store.Track
        return Object.keys(t).map(v => t[v])
      },
      albums: () => {
        return {albums: [
          store.store.Album[1],
          store.store.Album[2]
        ]}
      },
      volumes: () => {
        return {
          volumes: [
            store.store.Volume['0001'],
            store.store.Volume['0002']
          ],
          pager: {count: 2}
        }
      },
      latestVolumes: () => Object.keys(store.store.Volume).map(k => store.store.Volume[k])
    },
    Mutation: {
      addAlbum: (_, { album }, _context) => {
        store.get('Album', 2, 'name')
        store.set('Album', 2, {album_id: 2, name: album.name})
        return 2
      },
      deleteAlbumAlbum: (_, { id }) => {
        const found = store.store.Album[id]
        if(!found) throw Error('album not found')
        delete store.store.Album[id]

        return found
      },
      updateAlbumName: (_, { album_id, name }) => {
        let album = store.store.Album[album_id]
        store.set('Album', album_id, 'name', name)

        return album
      },
      updateAlbumArtist: (_, { album_id, artist }) => {
        store.store.Album[album_id].artist = artist
        return store.store.Album[album_id]
      },
      updateAlbumYear: (_, { album_id, year }) => {
        store.store.Album[album_id].tracks.forEach(({ $ref }) => {
          store.set('Track', $ref.key, 'year', year)
        })
        return store.store.Album[album_id]
      },
      updateTrackArtist: (_, _args) => {
      }
    }
  },
  mocks: {
    Thumbnail: () => 'http://thumbnail',
  }
})

export const client = new ApolloClient({
  cache: new InMemoryCache({
    dataIdFromObject: object => {
      switch (object.__typename) {
        case 'Album':
          // Album items in the cache will be keyed with Album1, Album2, etc, and items in ROOT_QUERY will contain references to these top level items
          return 'Album' + object.album_id
        default:
          return defaultDataIdFromObject(object)
      }
    }
  }),
  link: new SchemaLink({ schema })
})

export const Schema = schema
